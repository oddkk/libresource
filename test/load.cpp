#include "catch.hpp"
#include "resource/resource.h"
#include "resource/type.h"
#include "resource/manager.h"
#include "resource/threadinvoker.h"

struct Dummy {
	std::string val;
};


template<>
std::string resource::Type<Dummy>::_name = "dummy";

template<>
Dummy* resource::Type<Dummy>::load(Locator locator) {
	Dummy* d = new Dummy();
	d->val = "foo";
	return d;
}

template<>
void resource::Type<Dummy>::destroy(Dummy* data) {
	delete data;
}

template class resource::Type<Dummy>;


struct Dummy2 {
	std::string val;
	Dummy dummy;
};

template<>
std::string resource::Type<Dummy2>::_name = "dummy2";

template<>
Dummy2* resource::Type<Dummy2>::load(Locator locator) {
	Dummy2* d = new Dummy2();
	d->val = "bar";
	d->dummy = *resource::Resource<Dummy>(locator, resource::LoadSchedule::immediately).get();
	return d;
}

template<>
void resource::Type<Dummy2>::destroy(Dummy2* data) {
	delete data;
}

template class resource::Type<Dummy2>;

struct Dummy3 {
	std::string val;
};

template<>
std::string resource::Type<Dummy3>::_name = "dummy3";

void* dummy3_func(void* dt) {
	Dummy3* dummy = (Dummy3*)dt;
	dummy->val = "foobar";
	return nullptr;
}

template<>
Dummy3* resource::Type<Dummy3>::load(Locator locator) {
	Dummy3* d = new Dummy3();
	ThreadInvoker::invokeOnThread("LGC", dummy3_func, d).wait();
	return d;
}

template<>
void resource::Type<Dummy3>::destroy(Dummy3* data) {
	delete data;
}

template class resource::Type<Dummy3>;

TEST_CASE("Load dummy", "[load]") {
	resource::initialize();
	resource::ThreadInvoker invoker("LGC");
	SECTION("load dummy") {
		resource::Resource<Dummy> res("test", resource::LoadSchedule::immediately);
		REQUIRE(res.loaded());
		REQUIRE(res->val == "foo");
	}
	SECTION("load dummy2 (recurcive)") {
		resource::Resource<Dummy2> res("test2", resource::LoadSchedule::immediately);
		REQUIRE(res.loaded());
		REQUIRE(res->val == "bar");
		REQUIRE(res->dummy.val == "foo");
	}
	SECTION("load dummy3 (invoker)") {
		resource::Resource<Dummy3> res("test3", resource::LoadSchedule::immediately);
		REQUIRE(res.loaded());
		REQUIRE(res->val == "foobar");
	}
	resource::destroy();
}

