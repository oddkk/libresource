#include "catch.hpp"
#include "resource/locator.h"
#include <iostream>
#include <functional>

using namespace resource;

namespace std {
	using std::size_t;
	using std::hash;

	template<>
	struct hash<Locator>;
}

TEST_CASE("attribute", "[locator]") {
	SECTION("constructor") {
		SECTION("name value, constructor") {
			Attribute attr ("KEY", "VAL");
			REQUIRE(attr.name == "KEY");
			REQUIRE(attr.value == "VAL");
		}
		SECTION("attr string, constructor") {
			Attribute attr ("KEY=VAL");
			REQUIRE(attr.name == "KEY");
			REQUIRE(attr.value == "VAL");
		}
		SECTION("name value, value empty, constructor") {
			Attribute attr ("KEY", "");
			REQUIRE(attr.name == "KEY");
			REQUIRE(attr.value == "");
		}
		SECTION("attr string, value empty, no equals, constructor") {
			Attribute attr ("KEY");
			REQUIRE(attr.name == "KEY");
			REQUIRE(attr.value == "");
		}
		SECTION("attr string, value empty, constructor") {
			Attribute attr ("KEY=");
			REQUIRE(attr.name == "KEY");
			REQUIRE(attr.value == "");
		}
	}
	SECTION("to_string") {
		SECTION("regular") {
			Attribute attr("KEY1=VAL1");
			REQUIRE(attr.to_string() == "KEY1=VAL1");
		}
		SECTION("no value") {
			Attribute attr("KEY1=");
			REQUIRE(attr.to_string() == "KEY1");
		}
	}
}

TEST_CASE("attribute list", "[locator]") {
	SECTION("constructur") {
		SECTION("map, constructor") {
			std::map<std::string, std::string> map;
			map["KEY1"] = "VAL1";
			map["KEY2"] = "VAL2";
			map["KEY3"] = "VAL3";

			AttributeList list (map);

			REQUIRE(list.get("KEY1") == "VAL1");
			REQUIRE(list.get("KEY2") == "VAL2");
			REQUIRE(list.get("KEY3") == "VAL3");
		}
		SECTION("attribute vector, constructor") {
			std::vector<Attribute> attrs;
			attrs.push_back(Attribute("KEY1", "VAL1"));
			attrs.push_back(Attribute("KEY2", "VAL2"));
			attrs.push_back(Attribute("KEY3", "VAL3"));

			AttributeList list (attrs);

			REQUIRE(list.get("KEY1") == "VAL1");
			REQUIRE(list.get("KEY2") == "VAL2");
			REQUIRE(list.get("KEY3") == "VAL3");
		}
		SECTION("attr string, constructor") {
			AttributeList list ("KEY1=VAL1,KEY2=VAL2,KEY3=VAL3");

			REQUIRE(list.get("KEY1") == "VAL1");
			REQUIRE(list.get("KEY2") == "VAL2");
			REQUIRE(list.get("KEY3") == "VAL3");
		}
		SECTION("empty, add name value, constructor") {
			AttributeList list;

			list.set("KEY1", "VAL1");
			list.set("KEY2", "VAL2");
			list.set("KEY3", "VAL3");

			REQUIRE(list.get("KEY1") == "VAL1");
			REQUIRE(list.get("KEY2") == "VAL2");
			REQUIRE(list.get("KEY3") == "VAL3");
		}
		SECTION("empty, add attribute, constructor") {
			AttributeList list;

			list.set(Attribute("KEY1", "VAL1"));
			list.set(Attribute("KEY2", "VAL2"));
			list.set(Attribute("KEY3", "VAL3"));

			REQUIRE(list.get("KEY1") == "VAL1");
			REQUIRE(list.get("KEY2") == "VAL2");
			REQUIRE(list.get("KEY3") == "VAL3");
		}
	}
	SECTION("to_string") {
		std::string query;
		SECTION("single item") {
			query = "KEY1=VAL1";
		}
		SECTION("two item") {
			query = "KEY1=VAL1,KEY2=VAL2";
		}
		SECTION("three item") {
			query = "KEY1=VAL1,KEY2=VAL2,KEY3=VAL3";
		}
		AttributeList list (query);
		REQUIRE(list.to_string() == query);
	}
}

TEST_CASE("locator constructor", "[locator]") {
	SECTION("constructor") {
		SECTION("attributelist constructor") {
			Locator loc ("test.txt", AttributeList("KEY1=VAL1"), "type");
			REQUIRE(loc.name() == "test.txt");
			REQUIRE(loc.attributes().get("KEY1") == "VAL1");
			REQUIRE(loc.type() == "type");
		}
		SECTION("attrs string constructor") {
			Locator loc ("test.txt", "KEY1=VAL1", "type");
			REQUIRE(loc.name() == "test.txt");
			REQUIRE(loc.attributes().get("KEY1") == "VAL1");
			REQUIRE(loc.type() == "type");
		}
	}
	SECTION("to_string") {
		SECTION("to_string normal") {
			Locator loc ("test.txt", "KEY1=VAL1", "type");
			REQUIRE(loc.to_string() == "test.txt:KEY1=VAL1:type");
		}
		SECTION("to_string reversed attrs") {
			Locator loc ("test.txt", "KEY2=VAL2,KEY1=VAL1", "type");
			REQUIRE(loc.to_string() == "test.txt:KEY1=VAL1,KEY2=VAL2:type");
		}
	}
	SECTION("hash") {
		SECTION("hash equality") {
			Locator loc1;
			Locator loc2;
			SECTION("equal") {
				loc1 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type");
				loc2 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type");
			}
			SECTION("reversed attrs") {
				loc1 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type");
				loc2 = Locator("test.txt", "KEY2=VAL2,KEY1=VAL1", "type");
			}
			REQUIRE(std::hash<Locator>()(loc1) == std::hash<Locator>()(loc2));
		}
		SECTION("hash unequality") {
			Locator loc1;
			Locator loc2;
			SECTION("diffrent attrs") {
				loc1 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type");
				loc2 = Locator("test.txt", "KEY1=VAL2,KEY2=VAL1", "type");
			}
			SECTION("diffrent name") {
				loc1 = Locator("test1.txt", "KEY1=VAL1,KEY2=VAL2", "type");
				loc2 = Locator("test2.txt", "KEY1=VAL1,KEY2=VAL2", "type");
			}
			SECTION("diffrent type") {
				loc1 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type1");
				loc2 = Locator("test.txt", "KEY1=VAL1,KEY2=VAL2", "type2");
			}
			REQUIRE(std::hash<Locator>()(loc1) != std::hash<Locator>()(loc2));
		}
	}
}

