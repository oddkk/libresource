#ifndef LOADER_H
#define LOADER_H

#include <thread>
#include <atomic>
#include <mutex>
#include <queue>
#include "threadsafequeue.h"

namespace resource {
	class Instance;
	class Loader {
	public:
		Loader();

		void start();
		void stop();

		void queue(Instance* inst);

		void tick();
	private:
		std::atomic_bool _shouldRun;
		std::atomic_bool _isRunning;
		std::thread _thread;

		ThreadSafeQueue<Instance*> _queue;

		static void _run(Loader*);

	public:
		static bool isLoaderThread();
		static Loader instance;
	};
}

#endif
