#ifndef RESOURCE_RESOURCE_H
#define RESOURCE_RESOURCE_H

#include "locator.h"

namespace resource {
	class Instance;
	class _type;
	template<typename T> class Type;

	enum class LoadSchedule {
		immediately,
		queue,
		none
	};

	class _resource {
	public:
		_resource(Locator, LoadSchedule, _type*);

		void waitUntilLoaded() const;

		bool loaded() const;
		bool failed() const;

		const Locator locator;

	protected:
		void const* _get() const;

		Instance* _instance;
	};

	template<typename T>
	class Resource : public _resource {
	public:
		Resource(Locator locator,
		         LoadSchedule loadSchedule=LoadSchedule::queue)
			: _resource(locator, loadSchedule, &Type<T>::instance)
		{}
		Resource(std::string locator,
		         LoadSchedule loadSchedule=LoadSchedule::queue)
			: _resource(Locator(locator), loadSchedule, &Type<T>::instance)
		{}

		T const* operator->() const {
			return (T*)_get();
		}

		T const* get() const {
			return (T*)_get();
		}
	};
}

#endif
