#ifndef RESOURCE_THREADSAFEQUEUE_H
#define RESOURCE_THREADSAFEQUEUE_H

#include <queue>
#include "readwritelock.h"

namespace resource {
	template<typename T>
	class ThreadSafeQueue {
	public:
		void push(T val) {
			_queueLock.writeLock();
			_queue.push(std::move(val));
			_queueLock.writeUnlock();
		}
		T pop() {
			_queueLock.writeLock();
			T val = std::move(_queue.front());
			_queue.pop();
			_queueLock.writeUnlock();
			return std::move(val);
		}
		T& front() {
			_queueLock.readLock();
			T& val = _queue.front();
			_queueLock.readUnlock();
			return val;
		}
		bool empty() {
			_queueLock.readLock();
			bool val = _queue.empty();
			_queueLock.readUnlock();
			return val;
		}
		unsigned int size() {
			_queueLock.readLock();
			unsigned int val = _queue.size();
			_queueLock.readUnlock();
			return val;
		}

	private:
		std::queue<T> _queue;
		ReadWriteLock _queueLock;
	};
}

#endif
