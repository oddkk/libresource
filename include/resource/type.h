#ifndef RESOURCE_TYPE_H
#define RESOURCE_TYPE_H

#include "locator.h"
#include <exception>

namespace resource {
	class _type {
	public:
		virtual void* _load(Locator)=0;
		virtual void _destroy(void*)=0;

		virtual std::string name()=0;
	};
	template<typename T>
	class Type : public _type {
	public:
		T* load(Locator);
		void destroy(T*);

		virtual void* _load(Locator locator) {
			return (void*)load(locator);
		}
		virtual void _destroy(void* data) {
			destroy((T*)data);
		}
		virtual std::string name() {
			return Type<T>::_name;
		}

	public:
		static Type<T> instance;
		static std::string _name;
	};
	template<typename T>
	Type<T> Type<T>::instance = Type<T>();

	class LoadException : public std::exception {
		LoadException(std::string message);
		const std::string message;

		virtual const char* what() const throw() override;
	};
};

#endif
