#ifndef MANAGER_H
#define MANAGER_H

#include <string>
#include "locator.h"

namespace resource {
	void initialize();
	void destroy();

	extern std::string resourceDirectory;
}

#endif
