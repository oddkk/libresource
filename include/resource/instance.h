#ifndef INSTANCE_H
#define INSTANCE_H

#include "locator.h"
#include <set>

namespace resource {
	class _type;
	class Instance {
	public:
		~Instance();

		void load();

		void const* get() const;

		Locator locator() const;

		bool loading() const;
		bool loaded() const;
		bool failed() const;

	private:
		Instance(Locator, _type*);

		Locator _locator;
		void* _data;
		_type* _type;
		bool _loaded;
		bool _failed;
		bool _loading;

	public:
		static Instance* getInstance(Locator, resource::_type*);
	};
}

#endif
