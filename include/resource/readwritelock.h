#ifndef RESOURCE_READWRITELOCK_H
#define RESOURCE_READWRITELOCK_H

#include <atomic>

namespace resource {
	class ReadWriteLock {
	public:
		ReadWriteLock();
		void readLock();
		void readUnlock();

		void writeLock();
		void writeUnlock();

	private:
		std::atomic_bool _writeLock;
		std::atomic_bool _writeLockRequest;
		std::atomic_uint _readLock;
	};

}

#endif
