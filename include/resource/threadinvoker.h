#ifndef RESOURCE_THREADINVOKER_H
#define RESOURCE_THREADINVOKER_H

#include <string>
#include "threadsafequeue.h"
#include <future>
#include <memory>

namespace resource {
	typedef void*(*InvokeFunction)(void*);
	class ThreadInvoker {
	public:
		ThreadInvoker(std::string threadName);
		~ThreadInvoker();

		std::future<void*> invoke(InvokeFunction, void*);
		void tick();
		bool empty();

		const std::string threadName;

	private:
		struct Invokation {
			InvokeFunction function;
			void* data;
			std::promise<void*> promise;
		};

		ThreadSafeQueue<std::unique_ptr<Invokation>> _queue;

	public:
		static std::future<void*> invokeOnThread(std::string thread, InvokeFunction, void*);
		static void tickCurrentThread();
	};
}

#endif
