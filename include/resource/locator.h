#ifndef LOCATOR_H
#define LOCATOR_H

#include <iostream>
#include <string>
#include <vector>
#include <map>

namespace resource {
	struct Attribute {
		Attribute(std::string name, std::string value);
		Attribute(std::string attr);
		Attribute();
		std::string name, value;

		std::string to_string() const;
	};

	struct AttributeList {
		AttributeList();
		AttributeList(std::map<std::string, std::string>);
		AttributeList(std::vector<Attribute>);
		AttributeList(std::string);

		void set(std::string name, std::string value);
		void set(const Attribute&);
		std::string get(std::string name) const;

		std::string to_string() const;
	private:
		std::map<std::string, std::string> _attributes;
	};

	struct Locator {
		Locator();
		Locator(std::string name, AttributeList attrs=AttributeList(), std::string type="");
		Locator(std::string name, std::string attributes, std::string type="");
		Locator(Locator, std::string type);

		std::string name() const;
		AttributeList attributes() const;
		std::string type() const;
		std::string to_string() const;
		std::string filename() const;
		Locator keepOnlyName() const;

	private:
		std::string _name;
		AttributeList _attributes;
		std::string _type;
	};

	bool operator<(const Locator&, const Locator&);

	std::ostream& operator<<(std::ostream&, const Locator&);
	std::ostream& operator<<(std::ostream&, const Attribute&);
	std::ostream& operator<<(std::ostream&, const AttributeList&);
}

namespace std {
	template<>
	struct hash<resource::Locator> {
		std::size_t operator()(const resource::Locator& loc) const;
	};
}

#endif
