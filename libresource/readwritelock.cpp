#include "resource/readwritelock.h"
#include <thread>

namespace resource {
	ReadWriteLock::ReadWriteLock()
		: _writeLock(false),
		  _writeLockRequest(false),
		  _readLock(0)
	{}

	void ReadWriteLock::readLock() {
		while (_writeLock || _writeLockRequest)
			std::this_thread::yield();
		_readLock++;
	}

	void ReadWriteLock::readUnlock() {
		_readLock--;
	}

	void ReadWriteLock::writeLock() {
		_writeLockRequest = true;
		while (_readLock > 0)
			std::this_thread::yield();
		_writeLock = true;
		_writeLockRequest = false;
	}

	void ReadWriteLock::writeUnlock() {
		_writeLock = false;
	}
}
