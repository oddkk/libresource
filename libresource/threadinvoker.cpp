#include "resource/threadinvoker.h"
#include "resource/readwritelock.h"

#include <map>
#include <stdexcept>

namespace resource {
	static std::map<std::string, ThreadInvoker*> invokers;
	static ReadWriteLock invokersLock;
	static thread_local std::string threadName;

	ThreadInvoker::ThreadInvoker(std::string threadName)
		: threadName(threadName)
	{
		invokersLock.writeLock();
		invokers.emplace(threadName, this);
		invokersLock.writeUnlock();

		resource::threadName = threadName;
	}

	ThreadInvoker::~ThreadInvoker() {
		invokersLock.writeLock();
		invokers.erase(threadName);
		invokersLock.writeUnlock();
	}

	std::future<void*> ThreadInvoker::invoke(InvokeFunction func, void* data) {
		std::promise<void*> promise;
		Invokation* invoke = new Invokation{ func, data, std::move(promise) };
		_queue.push(std::unique_ptr<Invokation>(invoke));
		return invoke->promise.get_future();
	}

	void ThreadInvoker::tick() {
		if (!empty()) {
			std::unique_ptr<Invokation> func = _queue.pop();
			func->promise.set_value(func->function(func->data));
		}
	}

	bool ThreadInvoker::empty() {
		return _queue.empty();
	}

	static bool hasThreadInvoker(std::string name) {
		invokersLock.readLock();
		auto iter = invokers.find(name);
		bool res = (iter != invokers.end());
		invokersLock.readUnlock();
		return res;
	}

	static ThreadInvoker* getThreadInvoker(std::string name) {
		invokersLock.readLock();
		auto iter = invokers.find(name);
		if (iter == invokers.end()) {
			invokersLock.readUnlock();
			throw std::runtime_error("Cannot invoke function on thread '" + name + "'. The thread is not registered in resource");
		}
		ThreadInvoker* res = iter->second;
		invokersLock.readUnlock();
		return res;
	}

	std::future<void*> ThreadInvoker::invokeOnThread(std::string thread, InvokeFunction function, void* data) {
		return getThreadInvoker(thread)->invoke(function, data);
	}

	void ThreadInvoker::tickCurrentThread() {
		if (hasThreadInvoker(resource::threadName))
			getThreadInvoker(resource::threadName)->tick();
	}
}

