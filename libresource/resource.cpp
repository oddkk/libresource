#include "resource/resource.h"
#include "resource/instance.h"
#include "resource/manager.h"
#include "resource/type.h"
#include "resource/loader.h"
#include "resource/threadinvoker.h"

namespace resource {
	_resource::_resource(Locator loc, LoadSchedule sched, _type* type)
		: locator(loc, type->name()),
		  _instance(Instance::getInstance(locator, type)) {
		if (sched == LoadSchedule::immediately) {
			Loader::instance.queue(_instance);
			waitUntilLoaded();
		}
		else if (sched == LoadSchedule::queue) {
			Loader::instance.queue(_instance);
		}
	}

	void _resource::waitUntilLoaded() const {
		while (!_instance->loaded()) {
			if (Loader::isLoaderThread()) {
				Loader::instance.tick();
			}
			else {
				ThreadInvoker::tickCurrentThread();
			}
		}
	}

	void const* _resource::_get() const {
		return _instance->get();
	}

	bool _resource::loaded() const {
		return _instance->loaded();
	}

	bool _resource::failed() const {
		return _instance->failed();
	}

}

