#include "resource/instance.h"
#include "resource/type.h"

#include <memory>

namespace resource {
	static std::map<Locator, std::unique_ptr<Instance>> resources;

	Instance* Instance::getInstance(Locator locator, resource::_type* type) {
		auto iter = resources.find(locator);
		if (iter != resources.end())
			return iter->second.get();
		else {
			Instance* inst = new Instance(locator, type);
			resources.emplace(locator,
			                  std::unique_ptr<Instance>(inst));
			return inst;
		}
	}

	Instance::Instance(Locator locator, resource::_type* type)
		: _locator(locator),
		  _data(nullptr),
		  _type(type),
		  _loaded(false),
		  _failed(false),
		  _loading(false)
	{}

	Instance::~Instance() {
		if (loaded() && !failed())
			_type->_destroy(_data);
		_loaded = false;
	}

	void Instance::load() {
		if (_loading) return;
		_loading = true;
		try {
			_data = _type->_load(_locator);
			_failed = false;
		}
		catch (LoadException& ex) {
			_failed = true;
		}
		_loaded = true;
		_loading = false;
	}

	void const* Instance::get() const {
		return _data;
	}

	Locator Instance::locator() const {
		return _locator;
	}

	bool Instance::loading() const {
		return _loaded;
	}

	bool Instance::loaded() const {
		return _loaded;
	}

	bool Instance::failed() const {
		return _failed;
	}
}

