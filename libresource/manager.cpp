#include "resource/manager.h"
#include <thread>
#include <atomic>
#include <mutex>
#include <map>
#include <thread>
#include <chrono>

#include "resource/loader.h"
#include "resource/threadinvoker.h"

namespace resource {

	std::string resourceDirectory="";

	void initialize() {
		Loader::instance.start();
	}

	void destroy() {
		Loader::instance.stop();
	}
}

