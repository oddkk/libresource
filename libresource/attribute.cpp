#include "resource/locator.h"

namespace resource {
	static std::string readUntil(std::string::iterator& iter, const std::string::iterator& end, char separate) {
		std::string ret;
		for (; iter != end; iter++) {
			if (*iter == separate) { iter++; break; }
			ret += *iter;
		}
		return ret;
	}

	Attribute::Attribute(std::string name, std::string value)
		: name(name), value(value) {}

	Attribute::Attribute(std::string attr) {
		std::string::iterator iter = attr.begin();
		name = readUntil(iter, attr.end(), '=');
		value = readUntil(iter, attr.end(), '=');
	}

	Attribute::Attribute() {}

	std::string Attribute::to_string() const {
		return name + (value != "" ? "=" + value : "");
	}

	AttributeList::AttributeList() {}

	AttributeList::AttributeList(std::map<std::string, std::string> attrs)
		: _attributes(attrs) {}

	AttributeList::AttributeList(std::vector<Attribute> attrs) {
		for (unsigned int i = 0; i < attrs.size(); i++)
			_attributes[attrs[i].name] = attrs[i].value;
	}

	AttributeList::AttributeList(std::string attrs) {
		for (std::string::iterator iter = attrs.begin(); iter != attrs.end();) {
			Attribute attr (readUntil(iter, attrs.end(), ','));
			set(attr);
		}
	}

	void AttributeList::set(std::string name, std::string value) {
		_attributes[name] = value;
	}

	void AttributeList::set(const Attribute& attr) {
		_attributes[attr.name] = attr.value;
	}

	std::string AttributeList::get(std::string name) const {
		auto iter = _attributes.find(name);
		if (iter == _attributes.end()) return "";
		return iter->second;
	}

	std::string AttributeList::to_string() const {
		std::string res;
		for (auto iter = _attributes.begin(); iter != _attributes.end(); iter++)
			res += (res != "" ? "," : "") +
				Attribute(iter->first, iter->second).to_string();
		return res;
	}

	std::ostream& operator<<(std::ostream& o, const AttributeList& l) {
		return (o << l.to_string());
	}

	std::ostream& operator<<(std::ostream& o, const Attribute& l) {
		return (o << l.to_string());
	}
}

