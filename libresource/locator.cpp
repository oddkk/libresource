#include "resource/locator.h"
#include "resource/manager.h"
#include <iostream>
#include <functional>

namespace std {
	using std::size_t;
	using std::hash;

	std::size_t hash<resource::Locator>::operator()(const resource::Locator& loc) const {
		return std::hash<std::string>()(loc.to_string());
	}
	template struct hash<resource::Locator>;
}

namespace resource {
	Locator::Locator() {}

	Locator::Locator(std::string name, AttributeList attrs, std::string type)
		: _name(name), _attributes(attrs), _type(type) {}

	Locator::Locator(std::string name, std::string attrs, std::string type)
		: _name(name), _attributes(attrs), _type(type) {}

	Locator::Locator(Locator l, std::string type)
		: _name(l.name()), _attributes(l.attributes()), _type(type) {}

	std::string Locator::name() const {
		return _name;
	}

	AttributeList Locator::attributes() const {
		return _attributes;
	}

	std::string Locator::type() const {
		return _type;
	}

	std::string Locator::to_string() const {
		return name() + ":" +
			attributes().to_string() + ":" +
			type();
	}

	std::string Locator::filename() const {
		return resourceDirectory + name();
	}

	Locator Locator::keepOnlyName() const {
		return Locator(name());
	}

	bool operator<(const Locator& lhs, const Locator& rhs) {
		return std::hash<Locator>()(lhs) < std::hash<Locator>()(rhs);
	}

	std::ostream& operator<<(std::ostream& o, const Locator& l) {
		return (o << l.to_string());
	}
}

