#include "resource/type.h"

namespace resource {
	LoadException::LoadException(std::string message)
		: message(message)
	{}

	const char* LoadException::what() const throw() {
		return message.c_str();
	}
}
