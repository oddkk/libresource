#include "resource/type.h"
#include <vector>

namespace resource {
	template<>
	std::string Type<std::vector<char>>::_name = "data";

	template<>
	std::vector<char>* Type<std::vector<char>>::load(Locator locator) {
		return new std::vector<char>();
	}

	template<>
	void Type<std::vector<char>>::destroy(std::vector<char>* data) {
		delete data;
	}

	template class Type<std::vector<char>>;
}

