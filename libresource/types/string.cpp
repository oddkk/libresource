#include "resource/type.h"
#include "resource/resource.h"
#include <vector>
#include <string>

namespace resource {
	template<>
	std::string Type<std::string>::_name = "string";

	template<>
	std::string* Type<std::string>::load(Locator locator) {
		Resource<std::vector<char>> data(locator,
		                                 LoadSchedule::immediately);
		return new std::string(data->begin(), data->end());
	}

	template<>
	void Type<std::string>::destroy(std::string* data) {
		delete data;
	}

	template class Type<std::string>;
}

