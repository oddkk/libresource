#include "resource/loader.h"
#include "resource/instance.h"
#include <chrono>

namespace resource {
	static thread_local bool isLoaderThread = false;
	Loader Loader::instance;

	Loader::Loader()
		: _shouldRun(false),
		  _isRunning(false) {}

	void Loader::start() {
		_shouldRun = true;
		_isRunning = true;
		_thread = std::thread(_run, this);
	}

	void Loader::stop() {
		_shouldRun = false;
		_thread.join();
		_isRunning = false;
	}

	void Loader::queue(Instance* inst) {
		_queue.push(inst);
	}

	void Loader::tick() {
		if (!_queue.empty()) {
			Instance* instance = _queue.pop();
			if (!instance->loaded())
				instance->load();
		}
	}

	void Loader::_run(Loader* loader) {
		loader->_isRunning = true;
		resource::isLoaderThread = true;
		while (loader->_shouldRun) {
			if (loader->_queue.empty()) {
				std::this_thread::yield();
				continue;
			}
			loader->tick();
		}
		loader->_isRunning = false;
	}

	bool Loader::isLoaderThread() {
		return resource::isLoaderThread;
	}
}
